package com.vecturagames.android.app.passwordgenerator.enumeration;

public enum AppThemeType {

    LIGHT, DARK

}
