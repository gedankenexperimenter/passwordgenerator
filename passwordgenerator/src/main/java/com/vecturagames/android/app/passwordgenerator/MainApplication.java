package com.vecturagames.android.app.passwordgenerator;

import android.app.Application;
import android.content.Context;

public class MainApplication extends Application {

    private static Context mContext = null;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
    }

    public static Context getAppContext() {
        return mContext;
    }

}
